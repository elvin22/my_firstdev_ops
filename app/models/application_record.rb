# frozen_string_literal: true

Class ApplicationRecord < ActiveRecord::Base
self.abstract_class = true
End
